// Start sails and pass it command line arguments

process.env.HOST = process.env.OPENSHIFT_NODEJS_IP;
process.env.PORT = process.env.OPENSHIFT_NODEJS_PORT;
process.env.NODE_ENV = "produktion";

require('sails').lift(require('optimist').argv);
